package com.neuedu.aop;

import com.neuedu.AppTest;
import com.neuedu.mapper.UserMapper;
import com.neuedu.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: MARK
 * @Date: 2019/7/26 09:44
 * @version: 1.0.0
 * @Description:
 */

@Slf4j
public class exeTimeAopTest extends AppTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;
    @Test
    public void aroundMethod() {
        userService.add(665, 1);

    }
}