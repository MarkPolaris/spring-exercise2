package com.neuedu.service.impl;

import com.neuedu.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @Author: MARK
 * @Date: 2019/7/26 16:07
 * @version: 1.0.0
 * @Description:
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public Integer add(Integer a, Integer b) {
        return a + b;
    }
}
